# Planning Practice Activity

## Boards

Each Team has multiple boards for the system they are working on:

- One board for Epics under their system's GitLab subgroup
- There is a GitLab subgroup Issues board that aggregates all the issues from each of the projects under the subgroup
  - Each project in their system has a board for that project's issues

Boards are used to keep track of the status of individual Epics and Issues.

Boards consist of columns of cards representing Epics or Issues. These cards can be moved from column to column to represent an epic or issue changing its status.

## Backlog Refinement

---
---

**Read about [Backlog Refinement](https://www.agilealliance.org/glossary/backlog-refinement) to the end of the list of bullets.**

Your team's subgroup has some *Epics* that represent the *Product Owner's* requests for changes and new features. These epics are very high level, and could use some refinement.

---
---

### Epics

*Epics* in GitLab are part of a group or subgroup, not part of a project.

Epics are used for higher-level goals in a software development project. They could be based on a time period where multiple things will be accomplished. (Although a *milestone* is probably more appropriate for this.)

They can represent something that has to happen across multiple GitLab project. For example, if we were going to add a new field to the GuestInfo record, we might make this an epic because it involves modifying the API, the backend, and the frontend, and probably also modifying one or more parts of the Reporting system because it will now be receiving records with new information.

There would be multiple issues representing what has to be done to accomplish this - certainly at least one issue (and possibly more) in GuestInfoAPI, GuestInfoBackend, GuestInfoFrontend, ReportingSystemBackend, and maybe ReportingSystemAPI and ReportingSystemFrontend.

We could even make sub-epics in each of those projects if they are composed of multiple issues.

The epic is used to tie them all together and make it easier to track progress on all the issues.

We will use Epics for larger concepts. These will be things that need to be done that have multiple steps or tasks, leading to a larger project. Epics may even have sub-epics.

Epics typically cannot be completed in a single sprint, and will run across sprints. These might not be well defined yet, and will be refined into smaller epics and issues as the project progresses.

When refining an Epic, there is an `Add` button that will allow you to add linked sub-epics and issues.

Epics are attached to a GitLab subgroup.

---
---

**Go to the team's GitLab subgroup, and select `Plan > Epic Boards`.**

---
---

#### Epic Board Columns

The Epic board has the following columns:

- `Open` Epics that have not yet been started. You can use this as an "inbox" to add things as you think of them. The Epics here may be in the process of being refined by being broken down into smaller sub-epics and issues.
- `Status::In Process` Epics that are currently being worked on and have had some issues created representing concrete tasks to be completed.
- `Closed` Epics that have been completed, or that have been abandoned.

#### Column Headers

- Individual columns can be collapsed by clicking the `>` in the column header.
- Number of Epics is shown next to the "cards" icon.
- New Epics can be added by clicking the `+`.

#### Writing epics

- Title - Give the epic a good descriptive title.
- Description - Describe the epic and what needs to be done.
- Comments - Discuss the epic here. When a decision is made, edit the Description to reflect the decision. It is easier to look at the Description for the current status, than to have to scan through the comments and discussion. Use `@` mentions to notify team members, product owner, or customers to look at comments.

---
---

**Spend some time in your team working to refine the top three epics on your board:**

1. `Add a new endpoint to get just the major version of the API`
2. `Change POST /guests to PUT /guests`
3. `Change the Thea's Pantry background color`

These are the Product Owner's top three priorities in order of value to the Product Owner (at this time - this can and will change from sprint to sprint.)

Discuss these epics within your team, and come up with the steps that you think are necessary to complete them. Edit the description of the epics to include these steps. Don't forget to include which project the step needs to be completed on.

---
---

## Issues

Now that you have refined your epics and added steps, we can turn these steps or tasks into *Issues*.

*Issues* are part of a project.

We will use Issues for smaller tasks that can be completed in a single sprint. These have been defined well enough that one or more team members can take them on.

Most issues will be linked to an epic, as they represent tasks needed to complete that epic. But some issues will be stand-alone tasks.

Issues are attached to a GitLab project (repository).

---
---

**Go to the team's GitLab subgroup, and select `Plan > Issue Boards`.**

---
---

### Writing Issues

- Title - Give the issue a good descriptive title.
- Description - Describe the issue and what needs to be done.
- Projects - You must select a project within the system to attach the issue to. The `General` project can be used for broader issues that affect the team or cut across projects.
- Comments - Discuss the issue here. When a decision is made, edit the Description to reflect the decision. It is easier to look at the Description for the current status, than to have to scan through the comments and discussion. Use `@` mentions to notify team members, product owner, or customers to look at comments.

### Issue Board Columns

The board has the following columns:

- `Open` Issues that are not labeled. You can use this as an "inbox" to add things as you think of them, but they should not stay here long. If the issue seems like something that should be addressed, start refining it and move it to `Status::Product Backlog`. If many issues seem to be related, you may want to create an issue that they are linked to. Or if the issue seems to be too big to be completed in a single sprint, you may want to turn it in to an epic.
- `Status::Product Backlog` Sufficiently refined and sized that it could be taken up in a sprint, but was not yet accepted for a sprint in a planning meeting. Tasks should be SMART (see https://xp123.com/articles/invest-in-good-stories-and-smart-tasks/).
- `Status::Sprint Backlog` A task to do in the current sprint. Was selected by the team in the Sprint Planning meeting. 
- `Status::In Process` A claimed task actively being worked on. It has been assigned to one or more team members.
- `Status::Needs Review` The work for this issue needs a review by another team member before it can be merged or marked done.
- `Status::Done` Completed, but needs to be accepted by the Product Owner at (or after) the Review meeting.
- `Closed` Issues that have been completed or abandoned.

#### Column Headers

- Individual columns can be collapsed by clicking the `>` in the column header.
- Number of issues is shown next to the "cards" icon.
- Total weight of all issues is show next to the "weight" icon.
- New issues can be added by clicking the `+`. See *Issues* below.
- List settings can be accessed by clicking the "gear" icon if you want to set a work-in-progress limit for a column.

---
---

**Spend some time in your team working to break down the top three epics on your board into issues.**

Create at least one issue for each task in your epics. Make sure that you attach the issues to the appropriate project.

Make sure these issues appear on the Issues board in the order that they will likely be done. Make sure that they appear in the Product Owner's priority order.

---
---

## Issue Weights

Each issue that will be part of a sprint needs a weight so that the team can determine how many can be in a sprint.

- A weight of 0 might indicate that the task can be done immediately (often administrative tasks like creating a new project.)
- A weight of 1 might indicate that the task can be completed between one class and the next.
- A weight of 2 might indicate that the task will take a week.
- Weights higher than 2 are probably a bad idea. Try to break the issue down into smaller tasks.

### Determining Issue Weights (Planning Poker)

Because a Scrum team does not assign issues to team members before the beginning of a sprint, the whole team is involved in determining the weight of each issue. (In a Scrum team, any member of the team should be willing and able to pick up any issue that needs to be done if they are available to do it.)

To help the team come to agreement on the weights of issues, many teams use [Planning Poker](https://www.atlassian.com/blog/platform/a-brief-overview-of-planning-poker).

---
---

**Assign weights to the issues you created using Planning Poker.**

1. Read the issue out loud.
2. Discuss the issue
    - How will it be done?
    - How many people are needed?
    - What skills are needed?
3. Everyone privately selects a number (0, 1, 2, 3, 5, 8, 13, 20, 40, and 100).
    - In your team's Discord text channel - type your number, ***but do not hit enter yet***
4. Everyone reveals their number simultaneously.
    - When everyone is ready, hit enter
5. Reach consensus.
    - If all numbers match, you are done. **Adjust the weight on the issue.**
    - If not, discuss further. Often, the team members with the highest and lowest numbers will be asked to explain their choice.
    - Go to step 3.

Repeat for all issues that are ready for work.

---
---

## Sprint Planning

The goal for planning a sprint is to estimate how much work your team can sustainably accomplish during a single sprint (measured in *points*), and choose the highest-customer-value issues that add up to that number of points to commit to for the sprint.

### Number of Points in a Sprint (Velocity)

This value is arrived at over time based on the team's performance in previous sprints. The team constantly adjusts this number at the end of each sprint in an attempt to arrive at an accurate velocity value.

However, as a new team, we need a starting value. A possible starting velocity for a full-time developer is 1 point for an 8-hour day. So, the velocity for 1 developer for a 1-week sprint would be 5 points. Multiplying this by the number of members of your team gives you your team's velocity. (For team members working less than 4 hours per week, or who are out of the office for some reason, reduce their velocity appropriately.)

In our class, we are not full-time developers, so we need different values. We are going to set a starting velocity of 2 points per week for ourselves. Our first sprint is 2.5 weeks, so a single developer would have a velocity of 5 points per sprint.

We have 4-person teams, so the team velocity for a sprint will be 20 points. (Or 25 points for the one 5-person team.)

### Deciding on the Issues in a Sprint

Issues that are ready for work will be kept in the Product Backlog, in the order that the customer would like to see them completed, with a weight assigned to each.

Issues should be moved from the Product Backlog to the Sprint Backlog until the number of points of weight in the Sprint Backlog is equal to the team velocity.

---
---

**For this activity, your instructor will tell you how many points will be used.***

Move enough issues from the top of the list up to, but not over, the number of points your instructor assigned.

If you don't reach the number of points exactly, if you can find another issue or issues that fit in the remaining point *and doesn't depend on any issues not in the sprint** then add them.

---
---

&copy; 2024 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA
